sourceSets {
    getByName("main").java.srcDirs("src/main/kotlin")
    getByName("test").java.srcDirs("src/test/kotlin")
}

val grpcVersion = "1.36.0"
val exposedVersion = "0.31.1"
val koinVersion = "3.0.1"
val junitVersion = "5.3.1"

dependencies {
    implementation(project(":task-tracker-api"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation(group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-core", version = "1.4.3")
    implementation(group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-guava", version = "1.4.3")

    implementation("com.google.api.grpc:proto-google-common-protos:2.1.0")
    implementation("io.grpc:grpc-netty:${grpcVersion}")
    implementation("io.grpc:grpc-protobuf:${grpcVersion}")
    implementation("io.grpc:grpc-stub:${grpcVersion}")
    implementation("io.grpc:grpc-kotlin-stub:1.0.0")
    testImplementation("io.grpc:grpc-testing:${grpcVersion}")

    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposedVersion")

    implementation("org.postgresql:postgresql:42.2.20")

    implementation("io.insert-koin:koin-core:$koinVersion")
    testImplementation("io.insert-koin:koin-test:$koinVersion")
    testImplementation("io.insert-koin:koin-test-junit5:$koinVersion")

    implementation("org.slf4j:slf4j-simple:1.7.30")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:1.4.31")
}

tasks.test {
    useJUnitPlatform()
}

/** Creates fat Jar. */
val jar by tasks.getting(Jar::class) {
    archiveFileName.set(archiveBaseName.get() + ".jar")

    manifest {
        attributes("Main-Class" to "app.dreamteam.task_tracker.Main")
    }
    from({
        configurations.runtimeClasspath.get().map {
            if (it.isDirectory) it else zipTree(it)
        }
    })
    exclude("META-INF/*.RSA", "META-INF/*.SF", "META-INF/*.DSA")
}

/** Sets up ENV variables from file for tests. */
tasks.withType(Test::class) {
    doFirst {
        file("../example.env").readLines().forEach {
            if (it.contains("=") && !it.startsWith("#")) {
                val (key, value) = it.split("=", limit = 2)
                environment(key, value.replace(Regex("""^\"|\"${'$'}"""), ""))
            }
        }
    }
}