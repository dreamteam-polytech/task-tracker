package app.dreamteam.task_tracker.data.repositories

import app.dreamteam.task_tracker.api.TaskDTO
import java.util.*

object TaskObjectsCollection {
    val personCollection = PersonObjectsCollection

    val testTask1: TaskDTO by lazy {
        TaskDTO.newBuilder()
            .setUuid(UUID.randomUUID().toString())
            .setTitle("Test task 1")
            .setReporter(personCollection.testPerson1)
            .setAssignee(personCollection.testPerson2)
            .setDescription("TaskDTO description")
            .setStatus(TaskDTO.Status.IN_TEST)
            .build()
    }

    val testTask1Clone: TaskDTO by lazy {
        testTask1.toBuilder()
            .setTitle("Test task 1 clone")
            .build()
    }

    val testTask2: TaskDTO by lazy {
        TaskDTO.newBuilder()
            .setUuid(UUID.randomUUID().toString())
            .setTitle("Test task 2")
            .setReporter(personCollection.testPerson1)
            .setDescription("TaskDTO description")
            .setStatus(TaskDTO.Status.IN_TEST)
            .build()
    }

    val testTask3: TaskDTO by lazy {
        TaskDTO.newBuilder()
            .setUuid(UUID.randomUUID().toString())
            .setTitle("TaskDTO with complex name 3")
            .setReporter(personCollection.testPerson2)
            .setDescription("TaskDTO description")
            .setStatus(TaskDTO.Status.IN_TEST)
            .build()
    }
}