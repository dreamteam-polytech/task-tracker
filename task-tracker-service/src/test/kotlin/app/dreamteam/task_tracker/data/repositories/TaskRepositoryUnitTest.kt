package app.dreamteam.task_tracker.data.repositories

//@TestMethodOrder(OrderAnnotation::class)
//class TaskRepositoryUnitTest : TaskTrackerServer() {
//    private val personRepository: ContactRepository by inject()
//    private val taskRepository: TaskRepository by inject()
//    private val collection = TaskObjectsCollection
//
//    @Test @Order(1)
//    fun createTaskTest() {
//        val testTask = collection.testTask1
//        val createdTask = try { taskRepository.create(testTask) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testTask, createdTask)
//    }
//
//    @Test @Order(2)
//    fun readTaskTest() {
//        val testTask = collection.testTask1
//        val readTask = try { taskRepository.read(testTask) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testTask, readTask)
//    }
//
//    @Test @Order(3)
//    fun updateTaskTest() {
//        val testTask = collection.testTask1Clone
//        val updateTask = try { taskRepository.update(testTask) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testTask, updateTask)
//    }
//
//    @Test @Order(4)
//    fun deleteTaskTest() {
//        val testTask = collection.testTask1
//        try { cleaningUp(testTask) }
//        catch (t: Throwable) { fail(cause = t) }
//    }
//
//    @Test @Order(5)
//    fun createOrUpdateTaskTest() {
//        val testTask = collection.testTask1
//        val createTask = try { taskRepository.createOrUpdate(testTask) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testTask, createTask)
//
//        val testTaskClone = collection.testTask1Clone
//        val updateTask = try { taskRepository.createOrUpdate(testTaskClone) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testTaskClone, updateTask)
//
//        cleaningUp(testTask)
//    }
//
//    @Test @Order(6)
//    fun readOrCreateTaskTest() {
//        val testTask = collection.testTask2
//        val createTask = try { taskRepository.readOrCreate(testTask) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testTask, createTask)
//
//        val readTask = try { taskRepository.readOrCreate(testTask) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testTask, readTask)
//
//        cleaningUp(testTask)
//    }
//
//    @Test @Order(7)
//    fun findTaskTest() {
//        cleanTables()
//
//        val testTask1 = collection.testTask1
//        val testTask2 = collection.testTask2
//        val testTask3 = collection.testTask3
//
//        try {
//            taskRepository.readOrCreate(testTask1)
//            taskRepository.readOrCreate(testTask2)
//            taskRepository.readOrCreate(testTask3)
//        } catch (t: Throwable) {
//            fail(cause = t)
//        }
//
//        fun checkWithAsserts(
//            search: Task,
//            page: Int, pageSize: Int,
//            expectedPages: Int, expectedFound: Int
//        ) {
//            val request = GetTasks.Request.newBuilder()
//                .setBody(search)
//                .setPage(page).setPageSize(pageSize)
//                .build()
//            val response = try { taskRepository.find(request) }
//            catch (t: Throwable) { fail(cause = t) }
//
//            assertEquals(page, response.page)
//            assertEquals(pageSize, response.pageSize)
//            assertEquals(expectedPages, response.pagesNum)
//            assertEquals(expectedFound, response.tasksCount)
//        }
//
//        // Check search by title.
//        val search1 = Task.newBuilder().setTitle(testTask1.title.substringBefore(" ")).build()
//        checkWithAsserts(search1, 1, 2, 1, 2)
//
//        // Check search by description.
//        val search2 = Task.newBuilder().setDescription(testTask1.description).build()
//        checkWithAsserts(search2, 1, 2,  2, 2)
//
//        // Check search by status.
//        val search3 = Task.newBuilder().setStatus(testTask1.status).build()
//        checkWithAsserts(search3, 3, 3, 1, 0)
//        checkWithAsserts(search3, 1, 3, 1, 3)
//
//        // Check search by assignee.
//        val search4 = Task.newBuilder().setAssignee(testTask1.assignee).build()
//        checkWithAsserts(search4, 1, 10, 1, 1)
//
//        // Check search by reporter.
//        val search5 = Task.newBuilder().setReporter(testTask1.reporter).build()
//        checkWithAsserts(search5, 2, 1, 2, 1)
//
//        // Check complex search by multiple fields.
//        val search6 = Task.newBuilder()
//            .setStatus(testTask1.status)
//            .setReporter(testTask1.reporter)
//            .build()
//        checkWithAsserts(search6, 1, 10, 1, 2)
//
//        // Check search without restrictions.
//        val search7 = Task.newBuilder().build()
//        checkWithAsserts(search7, 1, 10, 1, 3)
//
//        cleanTables()
//    }
//
//    private fun cleaningUp(obj: Task) {
//        taskRepository.delete(obj)
//        try {
//            personRepository.delete(obj.reporter)
//        } catch (t: Throwable) {}
//
//        try {
//            if (obj.reporter != obj.assignee)
//                personRepository.delete(obj.assignee)
//        } catch (t: Throwable) {}
//    }
//
//    private fun cleanTables() {
//        loggerFind().info("Cleaning database...")
//        val n1 = taskRepository.deleteAll()
//        val n2 = personRepository.deleteAll()
//        loggerFind().info("${n1 + n2} rows was removed")
//    }
//}