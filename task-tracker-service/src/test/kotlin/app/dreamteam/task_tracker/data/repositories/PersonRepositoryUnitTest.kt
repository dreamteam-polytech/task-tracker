package app.dreamteam.task_tracker.data.repositories

//@TestMethodOrder(OrderAnnotation::class)
//class PersonRepositoryUnitTest : TaskTrackerServer() {
//    private val personRepository: ContactRepository by inject()
//    private val collection = PersonObjectsCollection
//
//    @Test @Order(1)
//    fun createPersonTest() {
//        val testPerson = collection.testPerson1
//        val createdPerson = try { personRepository.create(testPerson) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testPerson, createdPerson)
//    }
//
//    @Test @Order(2)
//    fun readPersonTest() {
//        val testPerson = collection.testPerson1
//        val readPerson = try { personRepository.read(testPerson) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testPerson, readPerson)
//    }
//
//    @Test @Order(3)
//    fun updatePersonTest() {
//        val testPerson = collection.testPerson1Clone
//        val updatePerson = try { personRepository.update(testPerson) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testPerson, updatePerson)
//    }
//
//    @Test @Order(4)
//    fun deletePersonTest() {
//        val testPerson = collection.testPerson1
//        try { cleaningUp(testPerson) }
//        catch (t: Throwable) { fail(cause = t) }
//    }
//
//    @Test @Order(5)
//    fun createOrUpdatePersonTest() {
//        val testPerson = collection.testPerson1
//        val createPerson = try { personRepository.createOrUpdate(testPerson) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testPerson, createPerson)
//
//        val testPersonClone = collection.testPerson1Clone
//        val updatePerson = try { personRepository.createOrUpdate(testPersonClone) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testPersonClone, updatePerson)
//
//        cleaningUp(testPerson)
//    }
//
//    @Test @Order(6)
//    fun readOrCreatePersonTest() {
//        val testPerson = collection.testPerson1
//        val createPerson = try { personRepository.readOrCreate(testPerson) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testPerson, createPerson)
//
//        val readPerson = try { personRepository.readOrCreate(testPerson) }
//        catch (t: Throwable) { fail(cause = t) }
//        assertEquals(testPerson, readPerson)
//
//        cleaningUp(testPerson)
//    }
//
//    private fun cleaningUp(obj: Person) {
//        personRepository.delete(obj)
//    }
//}