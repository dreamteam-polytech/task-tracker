package app.dreamteam.task_tracker.data.repositories

import app.dreamteam.task_tracker.api.ContactDTO
import kotlin.random.Random

object PersonObjectsCollection {
    val testPerson1: ContactDTO by lazy {
        ContactDTO.newBuilder()
            .setName("Test person 1")
            .setTamtamId(Random(System.nanoTime()).nextLong())
            .build()
    }

    val testPerson1Clone: ContactDTO by lazy {
        testPerson1.toBuilder()
            .setName("Test person 1 clone")
            .build()
    }

    val testPerson2: ContactDTO by lazy {
        ContactDTO.newBuilder()
            .setName("Test person 2")
            .setTamtamId(Random(System.nanoTime()).nextLong())
            .build()
    }
}