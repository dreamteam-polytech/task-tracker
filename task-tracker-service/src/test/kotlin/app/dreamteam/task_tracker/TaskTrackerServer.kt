package app.dreamteam.task_tracker

//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//abstract class TaskTrackerServer : KoinTest {
//    @BeforeAll
//    fun beforeAll() {
//        startKoin {
//            printLogger()
//            modules(mainModule)
//        }
//
//        initDatabase()
//    }
//
//    @AfterAll
//    fun afterAll() {
//        stopKoin()
//    }
//
//    private fun initDatabase() = try {
//        val database: Database = getKoin().get()
//        database.initConnection(
//            System.getenv("DB_URL"),
//            System.getenv("DB_USER"),
//            System.getenv("DB_PASS")
//        )
//        database.createTables()
//    } catch (t: Throwable) {
//        throw Throwable("Database connection error", t)
//    }
//}