package app.dreamteam.task_tracker

import org.jetbrains.exposed.sql.AbstractQuery
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.math.ceil

typealias APIException = io.grpc.StatusException
typealias APIStatus = io.grpc.Status

inline fun <reified T: Any> T.loggerFind(): Logger = LoggerFactory.getLogger(javaClass.canonicalName)

internal fun AbstractQuery<*>.countPages(page: Int, pageSizeRaw: Int): Triple<Int, Int, Long> {
    val totalCount = count()

    val pageSize = if (pageSizeRaw <= 0) 10 else pageSizeRaw
    val offset = (page - 1L) * pageSize

    val pagesTotal = ceil(totalCount.toDouble() / pageSize.toDouble()).toInt()
    return Triple(pageSize, pagesTotal, offset)
}

open class TTrackerException(override val message: String? = null) : Exception(message)
open class TTrackerDatabaseException(override val message: String? = null) : TTrackerException(message)
open class TTrackerNotFoundException(override val message: String? = null) : TTrackerDatabaseException(message)