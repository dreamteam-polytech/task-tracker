package app.dreamteam.task_tracker

import app.dreamteam.task_tracker.data.db.Database
import app.dreamteam.task_tracker.di.KoinLoggerAdapter
import app.dreamteam.task_tracker.di.coreModule
import app.dreamteam.task_tracker.di.mainModule
import org.koin.core.context.startKoin

object Main {
    /** Количество копий сообщений с одной и той же задачей для данного чата,
     * которые будут получать обновления при изменении в других местах. */
    const val updatingTasks = 1L

    private val logger = loggerFind()

    @JvmStatic
    fun main(args: Array<String>) {
        val koinApplication = startKoin {
            logger(KoinLoggerAdapter(logger))
            modules(mainModule, coreModule)
        }

        try {
            val database: Database = koinApplication.koin.get()
            database.initConnection(
                System.getenv("DB_URL"),
                System.getenv("DB_USER"),
                System.getenv("DB_PASS")
            )
            logger.info("Connected to the database")
            logger.info("Preparing DB workspace...")
            database.createTables()
            logger.info("The database is ready for use")
        } catch (t: Throwable) {
            throw Throwable("Database connection error", t)
        }

        val port = System.getenv("SERVICE_PORT")?.toInt() ?: 37555
        val server = TaskTrackerServer(port)
        server.start()
        server.blockUntilShutdown()
    }
}