package app.dreamteam.task_tracker.di

import app.dreamteam.task_tracker.core.mappers.operations.FindContactsMapper
import app.dreamteam.task_tracker.core.mappers.operations.FindTasksMapper
import app.dreamteam.task_tracker.core.mappers.operations.ManageTaskMessagesMapper
import app.dreamteam.task_tracker.data.db.Database
import app.dreamteam.task_tracker.data.mappers.ContactMapper
import app.dreamteam.task_tracker.data.mappers.TaskMapper
import app.dreamteam.task_tracker.data.repositories.ContactRepository
import app.dreamteam.task_tracker.data.repositories.TaskRepository
import app.dreamteam.task_tracker.data.repositories.impl.ContactRepositoryImpl
import app.dreamteam.task_tracker.data.repositories.impl.TaskRepositoryImpl
import org.koin.core.logger.KOIN_TAG
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE
import org.koin.dsl.module

val mainModule = module {
    single<TaskRepository> { TaskRepositoryImpl(get()) }
    single<ContactRepository> { ContactRepositoryImpl() }

    single { ContactMapper(get()) }
    single { TaskMapper(get(), get()) }

    single { Database(get(), get()) }
}

val coreModule = module {
    factory { app.dreamteam.task_tracker.core.mappers.entities.TaskMapper() }
    factory { FindTasksMapper.RequestMapper() }
    factory { FindTasksMapper.ResponseMapper() }

    factory { app.dreamteam.task_tracker.core.mappers.entities.ContactMapper() }
    factory { FindContactsMapper.RequestMapper() }
    factory { FindContactsMapper.ResponseMapper() }

    factory { ManageTaskMessagesMapper.RequestMapper() }
    factory { ManageTaskMessagesMapper.ResponseMapper() }
}

class KoinLoggerAdapter(private val base: org.slf4j.Logger) : Logger() {
    override fun log(level: Level, msg: MESSAGE) {
        val message = "$KOIN_TAG $msg"
        when (level) {
            Level.DEBUG -> base.debug(message)
            Level.INFO -> base.info(message)
            Level.ERROR -> base.error(message)
            Level.NONE -> base.debug(message)
        }
    }
}