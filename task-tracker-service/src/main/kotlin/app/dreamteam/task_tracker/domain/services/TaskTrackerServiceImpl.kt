package app.dreamteam.task_tracker.domain.services

import app.dreamteam.task_tracker.APIException
import app.dreamteam.task_tracker.APIStatus
import app.dreamteam.task_tracker.api.*
import app.dreamteam.task_tracker.core.TTrackerMapperException
import app.dreamteam.task_tracker.core.mappers.entities.ContactMapper
import app.dreamteam.task_tracker.core.mappers.entities.TaskMapper
import app.dreamteam.task_tracker.core.mappers.operations.FindContactsMapper
import app.dreamteam.task_tracker.core.mappers.operations.FindTasksMapper
import app.dreamteam.task_tracker.core.mappers.operations.ManageTaskMessagesMapper
import app.dreamteam.task_tracker.data.repositories.ContactRepository
import app.dreamteam.task_tracker.data.repositories.TaskRepository
import app.dreamteam.task_tracker.loggerFind
import com.google.protobuf.Empty
import com.google.protobuf.StringValue
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

// TODO: make database transactions suspend
class TaskTrackerServiceImpl : TaskTrackerGrpcKt.TaskTrackerCoroutineImplBase(), KoinComponent {
    private val logger = loggerFind()
    private val taskRepository: TaskRepository by inject()
    private val contactRepository: ContactRepository by inject()

    private val taskMapper: TaskMapper by inject()
    private val findTaskRequestMapper: FindTasksMapper.RequestMapper by inject()
    private val findTaskResponseMapper: FindTasksMapper.ResponseMapper by inject()

    private val contactMapper: ContactMapper by inject()
    private val findContactsRequestMapper: FindContactsMapper.RequestMapper by inject()
    private val findContactsResponseMapper: FindContactsMapper.ResponseMapper by inject()

    private val manageTaskMessagesRequestMapper: ManageTaskMessagesMapper.RequestMapper by inject()
    private val manageTaskMessagesResponseMapper: ManageTaskMessagesMapper.ResponseMapper by inject()

    override suspend fun createTask(request: TaskDTO): Empty = safe {
        val mapped = taskMapper.mapIn(request)
        taskRepository.create(mapped)
        Empty.getDefaultInstance()
    }

    override suspend fun readTask(request: StringValue): TaskDTO = safe {
        val uuid = UUID.fromString(request.value)
        val value = taskRepository.read(uuid)
        taskMapper.mapOut(value)
    }

    override suspend fun updateTask(request: TaskDTO): Empty = safe {
        val mapped = taskMapper.mapIn(request)
        taskRepository.update(mapped)
        Empty.getDefaultInstance()
    }

    override suspend fun deleteTask(request: StringValue): Empty = safe {
        val uuid = UUID.fromString(request.value)
        taskRepository.delete(uuid)
        Empty.getDefaultInstance()
    }

    override suspend fun updateOrCreateTask(request: TaskDTO): Empty = safe {
        val mapped = taskMapper.mapIn(request)
        taskRepository.createOrUpdate(mapped)
        Empty.getDefaultInstance()
    }

    override suspend fun findTasks(request: FindTasksDTO.Request): FindTasksDTO.Response = safe {
        val mapped = findTaskRequestMapper.mapIn(request)
        val result = taskRepository.find(mapped)
        findTaskResponseMapper.mapOut(result)
    }

    override suspend fun updateOrCreateContact(request: ContactDTO): Empty = safe {
        val mapped = contactMapper.mapIn(request)
        contactRepository.createOrUpdate(mapped)
        Empty.getDefaultInstance()
    }

    override suspend fun findContacts(request: FindContactsDTO.Request): FindContactsDTO.Response = safe {
        val mapped = findContactsRequestMapper.mapIn(request)
        val result = contactRepository.find(mapped)
        findContactsResponseMapper.mapOut(result)
    }

    override suspend fun manageTaskMessages(request: ManageTaskMessagesDTO.Request): ManageTaskMessagesDTO.Response = safe {
        val mapped = manageTaskMessagesRequestMapper.mapIn(request)
        val result = taskRepository.manageTaskMessages(mapped)
        manageTaskMessagesResponseMapper.mapOut(result)
    }

    private inline fun <R> safe(action: () -> R): R {
        return try {
            action()
        } catch (t: TTrackerMapperException) {
            logger.error("DTO mapping exception. You need to fix this", t)
            throw APIException(APIStatus.INTERNAL)
        } catch (t: Throwable) {
            logger.error("Caught exception: ", t)
//            logger.warn("Exception handled: ${t.message}. " +
//                    "Cause: ${t.cause?.message?.substringBefore('\n')}")
            throw t
        }
    }
}