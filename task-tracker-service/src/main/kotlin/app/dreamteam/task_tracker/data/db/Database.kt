package app.dreamteam.task_tracker.data.db

import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.data.repositories.ContactRepository
import app.dreamteam.task_tracker.data.repositories.TaskRepository
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.koin.core.component.KoinComponent
import java.time.LocalDateTime

object Tables {
    object Contacts : LongIdTable() {
        val name = varchar("name", 50)
        val tamtamId = long("tamtam_id").uniqueIndex()
        val tamtamChatId = long("tamtam_chat_id").uniqueIndex().nullable()
    }

    object Tasks : UUIDTable() {
        val title = varchar("title", 300).index()
        val reporter = reference("reporter", Contacts, onDelete = ReferenceOption.SET_NULL, onUpdate = ReferenceOption.CASCADE).index()
        var assignee = optReference("assignee", Contacts, onDelete = ReferenceOption.SET_NULL, onUpdate = ReferenceOption.CASCADE).index()
        val status = enumerationByName("status", 30, Task.Status::class).index()
        val description = text("description", eagerLoading = true).nullable()
        val timeCreated = datetime("time_created").clientDefault { LocalDateTime.now() }.index()
        val timeUpdated = datetime("time_updated").clientDefault { LocalDateTime.now() }.index()
    }

    object TaskMessages : UUIDTable() {
        val taskId = reference("task", Tasks, onDelete = ReferenceOption.CASCADE, onUpdate = ReferenceOption.CASCADE)
        val tamtamChatId = long("tamtam_chat_id").index()
        val tamtamMessageId = varchar("tamtam_message_id", 50).index()
        val timeCreated = datetime("time_created").clientDefault { LocalDateTime.now() }.index()

        init {
            index(true, taskId, tamtamMessageId)
        }
    }
}

class Database(
    private val taskRepository: TaskRepository,
    private val contactRepository: ContactRepository
) : KoinComponent {
    fun initConnection(url: String, user: String, password: String) {
        val db = Database.connect(url, user = user, password = password)
        //db.useNestedTransactions = true
    }

    fun createTables() {
        taskRepository.createTable()
        contactRepository.createTable()
    }
}

fun <T : Comparable<T>> Entity<T>.get(t: T) = this.klass[t]