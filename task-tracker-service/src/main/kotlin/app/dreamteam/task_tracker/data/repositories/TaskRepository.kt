package app.dreamteam.task_tracker.data.repositories

import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.core.operations.FindTasks
import app.dreamteam.task_tracker.core.operations.ManageTaskMessages
import app.dreamteam.task_tracker.data.db.Tables
import app.dreamteam.task_tracker.data.db.TaskEntity
import app.dreamteam.task_tracker.data.mappers.TaskMapper
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.inject
import java.util.*

abstract class TaskRepository(
    protected val personRepository: ContactRepository
) : BaseRepository<UUID, TaskEntity, Task>() {
    override val mapper: TaskMapper by inject()

    override fun deleteAll(): Int = transaction { Tables.Tasks.deleteAll() }

    internal abstract fun find(obj: FindTasks.Request): FindTasks.Response

    internal abstract fun manageTaskMessages(request: ManageTaskMessages.Request): ManageTaskMessages.Response
}