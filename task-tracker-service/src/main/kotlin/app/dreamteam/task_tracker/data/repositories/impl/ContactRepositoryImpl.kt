package app.dreamteam.task_tracker.data.repositories.impl

import app.dreamteam.task_tracker.TTrackerNotFoundException
import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.operations.FindContacts
import app.dreamteam.task_tracker.countPages
import app.dreamteam.task_tracker.data.db.ContactEntity
import app.dreamteam.task_tracker.data.db.Tables
import app.dreamteam.task_tracker.data.repositories.ContactRepository
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.andWhere
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.KoinComponent

class ContactRepositoryImpl : ContactRepository(), KoinComponent {
    override fun createTable() = transaction {
        SchemaUtils.create(Tables.Contacts)
    }

    override fun createEntity(obj: Contact): ContactEntity = transaction {
        mapper.mapCreate(obj)
    }

    override fun readEntity(id: Long): ContactEntity = transaction {
        ContactEntity[id]
    }

    override fun updateEntity(obj: Contact): ContactEntity = transaction {
        mapper.mapUpdate(obj)
    }

    override fun deleteEntity(id: Long) = transaction {
        readEntity(id).delete()
    }

    override fun readEntityByTamtamId(tamtamId: Long): ContactEntity = transaction {
        ContactEntity.find { Tables.Contacts.tamtamId eq tamtamId }.limit(1).firstOrNull()
             ?: throw TTrackerNotFoundException("Not found entity with 'tamtamId' == $tamtamId")
    }

    override fun readByTamtamId(tamtamId: Long): Contact = transaction {
        mapper.map(readEntityByTamtamId(tamtamId))
    }

    override fun readOrCreateEntityByTamtamId(tamtamId: Long, obj: Contact): ContactEntity = transaction {
        try { readEntityByTamtamId(tamtamId) } catch (t: Throwable) { createEntity(obj) }
    }

    override fun readOrCreateByTamtamId(tamtamId: Long, obj: Contact): Contact = transaction {
         mapper.map(readOrCreateEntityByTamtamId(tamtamId, obj))
    }

    override fun find(obj: FindContacts.Request): FindContacts.Response = transaction {
        val query = Tables.Contacts.selectAll()

        obj.body.name?.let {
            query.andWhere { Tables.Contacts.name like "%$it%" }
        }

        obj.body.tamtamId?.let {
            query.andWhere { Tables.Contacts.tamtamId eq it }
        }

        obj.body.tamtamChatId?.let {
            query.andWhere { Tables.Contacts.tamtamChatId eq it }
        }

        val (pageSize, pagesTotal, offset) = query.countPages(obj.page, obj.pageSize)
        query.limit(pageSize, offset = offset)

        // DEBUG
        //logger.info(query.prepareSQL(this))

        val queryResult = query.map { mapper.map(ContactEntity.wrapRow(it)) }
        FindContacts.Response(
            obj.page,
            pageSize,
            pagesTotal,
            queryResult
        )
    }
}