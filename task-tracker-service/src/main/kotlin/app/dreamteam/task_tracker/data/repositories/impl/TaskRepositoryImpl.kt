package app.dreamteam.task_tracker.data.repositories.impl

import app.dreamteam.task_tracker.Main
import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.core.operations.FindTasks
import app.dreamteam.task_tracker.core.operations.ManageTaskMessages
import app.dreamteam.task_tracker.countPages
import app.dreamteam.task_tracker.data.db.Tables
import app.dreamteam.task_tracker.data.db.TaskEntity
import app.dreamteam.task_tracker.data.repositories.ContactRepository
import app.dreamteam.task_tracker.data.repositories.TaskRepository
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

class TaskRepositoryImpl(personRepository: ContactRepository) : TaskRepository(personRepository) {
    override fun createTable() = transaction {
        SchemaUtils.create(Tables.Tasks, Tables.TaskMessages)
    }

    override fun createEntity(obj: Task): TaskEntity = transaction {
        mapper.mapCreate(obj)
    }

    override fun readEntity(id: UUID): TaskEntity = transaction {
        TaskEntity[id]
    }

    override fun updateEntity(obj: Task): TaskEntity = transaction {
        mapper.mapUpdate(obj)
    }

    override fun deleteEntity(id: UUID) = transaction {
        readEntity(id).delete()
    }

    override fun find(obj: FindTasks.Request): FindTasks.Response = transaction {
        val contactsR = Tables.Contacts.alias("contacts_r")
        val contactsA = Tables.Contacts.alias("contacts_a")
        val query = Tables.Tasks
            .leftJoin(contactsR, { Tables.Tasks.reporter }, { contactsR[Tables.Contacts.id] })
            .leftJoin(contactsA, { Tables.Tasks.assignee }, { contactsA[Tables.Contacts.id] })
            .select {
                (contactsR[Tables.Contacts.tamtamId] eq obj.sender.tamtamId) or
                        (contactsA[Tables.Contacts.tamtamId] eq obj.sender.tamtamId)
            }.orderBy(Tables.Tasks.timeCreated)

        obj.request.uuid?.let {
            query.andWhere { Tables.Tasks.id eq it }
        }

        obj.request.title?.let {
            if (it.isNotBlank())
                query.andWhere { Tables.Tasks.title like "%$it%" }
        }

        obj.request.reporter?.let {
            query.andWhere { contactsR[Tables.Contacts.tamtamId] eq it.tamtamId }
        }

        obj.request.assignee?.let {
            query.andWhere { contactsA[Tables.Contacts.tamtamId] eq it.tamtamId }
        }

        obj.request.status?.let {
            query.andWhere { Tables.Tasks.status eq it }
        }

        obj.request.description?.let {
            if (it.isNotBlank())
                query.andWhere { Tables.Tasks.description like "%$it%" }
        }

        val (pageSize, pagesTotal, offset) = query.countPages(obj.page, obj.pageSize)
        query.limit(pageSize, offset = offset)

        // DEBUG
        //logger.info(query.prepareSQL(this))

        val queryResult = query.map { mapper.map(TaskEntity.wrapRow(it)) }
        FindTasks.Response(
            obj.page,
            pageSize,
            pagesTotal,
            queryResult
        )
    }

    override fun manageTaskMessages(request: ManageTaskMessages.Request): ManageTaskMessages.Response = transaction {
        when (request) {
            is ManageTaskMessages.Request.GetMessages -> {
                val query = Tables.TaskMessages.select {
                    Tables.TaskMessages.taskId eq request.taskId
                }

                val result = query.map { it[Tables.TaskMessages.tamtamMessageId] }
                ManageTaskMessages.Response.GetMessages(request.taskId, result)
            }
            is ManageTaskMessages.Request.PinMessages -> {
                Tables.TaskMessages.batchInsert(
                    request.chatIds_messageIds,
                    shouldReturnGeneratedValues = false
                ) { (chatId, messageId) ->
                    this[Tables.TaskMessages.taskId] = request.taskId
                    this[Tables.TaskMessages.tamtamChatId] = chatId
                    this[Tables.TaskMessages.tamtamMessageId] = messageId
                }

                /* Для каждой пары <задача, чат> оставляем не более [updatingTasks]
                 * привзяанных сообщений. */
                val unpinnedMessagesIds = mutableListOf<String>()
                val chatIds = request.chatIds_messageIds.map { it.first }

                // Ищем ID сообщений для отмены привязки к задаче.
                chatIds.forEach { chatId ->
                    val unpinQuery = Tables.TaskMessages.select {
                        (Tables.TaskMessages.taskId eq request.taskId) and (Tables.TaskMessages.tamtamChatId eq chatId)
                    }.orderBy(Tables.TaskMessages.timeCreated, SortOrder.DESC)
                        .limit(Int.MAX_VALUE, offset = Main.updatingTasks)

                    logger.info(unpinQuery.prepareSQL(this))

                    val forUnpinMessageIds = unpinQuery.map { it[Tables.TaskMessages.tamtamMessageId] }
                    unpinnedMessagesIds.addAll(forUnpinMessageIds)
                }

                // Удаляем эти привязки.
                Tables.TaskMessages.deleteWhere {
                    Tables.TaskMessages.tamtamMessageId inList unpinnedMessagesIds
                }

                ManageTaskMessages.Response.PinMessages(request.taskId, unpinnedMessagesIds)
            }
            is ManageTaskMessages.Request.UnpinMessages -> {
                request.messageIds.forEach { messageId ->
                    Tables.TaskMessages.deleteIgnoreWhere {
                        Tables.TaskMessages.tamtamMessageId eq messageId
                    }
                }

                ManageTaskMessages.Response.UnpinMessages(request.taskId)
            }
        }
    }
}