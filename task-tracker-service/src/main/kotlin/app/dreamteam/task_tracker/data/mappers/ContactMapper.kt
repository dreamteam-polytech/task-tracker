package app.dreamteam.task_tracker.data.mappers

import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.data.db.ContactEntity
import app.dreamteam.task_tracker.data.repositories.ContactRepository

class ContactMapper(
    override val baseRepo: ContactRepository,
    override val actionReadDefault: (Contact) -> ContactEntity = { baseRepo.readEntityByTamtamId(it.tamtamId) }
) : DBMapper<ContactEntity, Contact> {
    override fun map(input: ContactEntity): Contact {
        return Contact(
            input.name,
            input.tamtamId,
            input.tamtamChatId
        )
    }

    override fun mapUpdate(input: Contact, actionRead: (Contact) -> ContactEntity): ContactEntity {
        return map(input, actionRead(input))
    }

    override fun mapCreate(input: Contact): ContactEntity {
        return ContactEntity.new { map(input, this) }
    }

    private fun map(input: Contact, target: ContactEntity): ContactEntity {
        target.name = input.name
        target.tamtamId = input.tamtamId
        target.tamtamChatId = input.tamtamChatId
        return target
    }
}
