package app.dreamteam.task_tracker.data.repositories

import app.dreamteam.task_tracker.data.mappers.DBMapper
import app.dreamteam.task_tracker.loggerFind
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.KoinComponent

abstract class BaseRepository<ID, I : Entity<*>, O> : CrudRepository<ID, O>, KoinComponent {
    val logger = loggerFind()
    abstract val mapper: DBMapper<I, O>

    abstract fun createTable()

    internal abstract fun deleteAll(): Int

    internal abstract fun createEntity(obj: O): I
    internal abstract fun readEntity(id: ID): I
    internal abstract fun updateEntity(obj: O): I
    internal abstract fun deleteEntity(id: ID)

    internal fun readOrCreateEntity(id: ID, obj: O): I = transaction {
        try { readEntity(id) } catch (t: Throwable) { createEntity(obj) }
    }

    internal fun createOrUpdateEntity(obj: O): I = transaction {
        try { updateEntity(obj) } catch (t: Throwable) { createEntity(obj) }
    }

    override fun create(obj: O): O = transaction { mapper.map(createEntity(obj)) }
    override fun read(id: ID): O = transaction { mapper.map(readEntity(id)) }
    override fun update(obj: O): O = transaction { mapper.map(updateEntity(obj)) }
    override fun delete(id: ID) = transaction { deleteEntity(id) }

    fun readOrCreate(id: ID, obj: O): O = transaction { mapper.map(readOrCreateEntity(id, obj)) }
    fun createOrUpdate(obj: O): O = transaction { mapper.map(createOrUpdateEntity(obj)) }
}