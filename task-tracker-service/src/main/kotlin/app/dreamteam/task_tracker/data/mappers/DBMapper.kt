package app.dreamteam.task_tracker.data.mappers

import app.dreamteam.task_tracker.data.repositories.BaseRepository
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.sql.transactions.transaction

interface DBMapper<I : Entity<*>, O> {
    /** Нужна для методов чтения/создания сущностей во время маппинга. */
    val baseRepo: BaseRepository<*, I, O>
    val actionReadDefault: (O) -> I

    fun map(input: I): O

    fun mapUpdate(input: O, actionRead: (O) -> I = actionReadDefault): I
    fun mapCreate(input: O): I

    fun mapUpdateOrCreate(input: O, actionRead: (O) -> I = actionReadDefault) = transaction {
        try {
            mapUpdate(input, actionRead)
        } catch (t: Throwable) {
            mapCreate(input)
        }
    }

    fun mapReadOrCreate(input: O, actionRead: (O) -> I = actionReadDefault) = transaction {
        try {
            actionRead(input)
        } catch (t: Throwable) {
            mapCreate(input)
        }
    }
}