package app.dreamteam.task_tracker.data.db

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import java.util.*

class ContactEntity(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<ContactEntity>(Tables.Contacts)

    var name by Tables.Contacts.name
    var tamtamId by Tables.Contacts.tamtamId
    var tamtamChatId by Tables.Contacts.tamtamChatId
}

class TaskEntity(uuid: EntityID<UUID>) : UUIDEntity(uuid) {
    companion object : UUIDEntityClass<TaskEntity>(Tables.Tasks)

    var title by Tables.Tasks.title
    var description by Tables.Tasks.description
    var status by Tables.Tasks.status
    var reporter by ContactEntity referencedOn Tables.Tasks.reporter
    var assignee by ContactEntity optionalReferencedOn Tables.Tasks.assignee
    var timeCreated by Tables.Tasks.timeCreated
    var timeUpdated by Tables.Tasks.timeUpdated
}

class TaskMessageEntity(uuid: EntityID<UUID>) : UUIDEntity(uuid) {
    companion object : UUIDEntityClass<TaskMessageEntity>(Tables.TaskMessages)

    var task by TaskEntity referencedOn Tables.TaskMessages.taskId
    var tamtamChatId by Tables.TaskMessages.tamtamChatId
    var tamtamMessageId by Tables.TaskMessages.tamtamMessageId
    var timeCreated by Tables.TaskMessages.timeCreated
}