package app.dreamteam.task_tracker.data.repositories

import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.operations.FindContacts
import app.dreamteam.task_tracker.data.db.ContactEntity
import app.dreamteam.task_tracker.data.db.Tables
import app.dreamteam.task_tracker.data.mappers.ContactMapper
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.inject

abstract class ContactRepository : BaseRepository<Long, ContactEntity, Contact>() {
    override val mapper: ContactMapper by inject()

    override fun deleteAll(): Int = transaction { Tables.Contacts.deleteAll() }

    internal abstract fun readEntityByTamtamId(tamtamId: Long): ContactEntity
    internal abstract fun readByTamtamId(tamtamId: Long): Contact

    internal abstract fun readOrCreateEntityByTamtamId(tamtamId: Long, obj: Contact): ContactEntity
    internal abstract fun readOrCreateByTamtamId(tamtamId: Long, obj: Contact): Contact

    internal abstract fun find(obj: FindContacts.Request): FindContacts.Response
}