package app.dreamteam.task_tracker.data.mappers

import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.data.db.TaskEntity
import app.dreamteam.task_tracker.data.repositories.TaskRepository
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime

class TaskMapper(
    private val contactMapper: ContactMapper,
    override val baseRepo: TaskRepository,
    override val actionReadDefault: (Task) -> TaskEntity = { baseRepo.readEntity(it.uuid) }
) : DBMapper<TaskEntity, Task> {
    override fun map(input: TaskEntity): Task {
        return Task(
            input.title,
            contactMapper.map(input.reporter),
            input.assignee?.let { contactMapper.map(it) },
            input.status,
            input.description,
            input.id.value
        )
    }

    override fun mapUpdate(input: Task, actionRead: (Task) -> TaskEntity): TaskEntity {
        return map(input, actionRead(input))
    }

    override fun mapCreate(input: Task): TaskEntity {
        return TaskEntity.new(input.uuid) { map(input, this) }
    }

    private fun map(input: Task, entity: TaskEntity): TaskEntity = transaction {
        entity.title = input.title
        entity.status = input.status
        entity.description = input.description
        entity.timeUpdated = LocalDateTime.now()
        try { entity.timeCreated } // This code used both in update/create, so.
        catch (t: IllegalStateException) { entity.timeCreated = entity.timeUpdated }

        entity.reporter = contactMapper.mapReadOrCreate(input.reporter)
        entity.assignee = input.assignee?.let { contactMapper.mapReadOrCreate(it) }

        return@transaction entity
    }
}

