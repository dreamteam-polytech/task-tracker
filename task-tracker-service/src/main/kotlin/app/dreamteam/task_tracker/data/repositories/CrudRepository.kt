package app.dreamteam.task_tracker.data.repositories

interface CrudRepository<ID, T> {
    fun create(obj: T): T
    fun read(id: ID): T
    fun update(obj: T): T
    fun delete(id: ID)
}