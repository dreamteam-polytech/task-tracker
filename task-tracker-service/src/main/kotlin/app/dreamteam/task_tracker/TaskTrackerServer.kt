package app.dreamteam.task_tracker

import app.dreamteam.task_tracker.domain.services.TaskTrackerServiceImpl
import app.dreamteam.task_tracker.domain.services.interseptors.UnknownStatusInterceptor
import io.grpc.Server
import io.grpc.ServerBuilder
import io.grpc.ServerInterceptors

class TaskTrackerServer(private val port: Int) {
    private val logger = loggerFind()

    private val server: Server by lazy {
        ServerBuilder
            .forPort(port)
            .addService(ServerInterceptors.intercept(TaskTrackerServiceImpl(), UnknownStatusInterceptor()))
            .build()
    }

    fun start() {
        server.start()
        logger.info("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                logger.warn("*** shutting down gRPC server since JVM is shutting down")
                this@TaskTrackerServer.stop()
                logger.warn("*** server shut down")
            }
        )
    }

    private fun stop() {
        server.shutdown()
    }

    @Throws(InterruptedException::class)
    fun blockUntilShutdown() {
        server.awaitTermination()
    }
}